const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 8000;
const nunjucks = require("nunjucks");
const routes = require("./routes/routes");


app.use(cors());
app.use(express.static("public"));
app.use(express.json());


nunjucks.configure( 'src/views', {
    autoescape: true,
    cache: false,
    express: app
});


mongoose.connect("mongodb://127.0.0.1:27017/todo").then(() => {
	console.log("Mongoose connected!");
	app.use("/", routes());
	app.listen(PORT, () => {
		console.log("Server started at http://localhost:" + PORT);
	});
});





