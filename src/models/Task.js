const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TaskSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum : ['new', 'complete'],
        default: 'new',
        required: true
    },
});

const Task = mongoose.model("Task", TaskSchema);
module.exports = Task;
