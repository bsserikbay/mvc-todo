const express = require("express");
const router = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const {getTasks} = require("../controllers/getTasks");
const {postTask} = require("../controllers/postTask");
const {deleteTask} = require("../controllers/deleteTask");
const {editTask} = require("../controllers/editTask");

const createRouter = () => {

    router.get('/',  function(req, res) {
        getTasks(req, function (err, result) {
            if (err)
                return (err);
            // console.log('DB result: ', result);
            res.render('index.html', {tasks: result})
        }).then()
    });

    router.get('/new',  function(req, res) {
        res.render('new.html')
    });

    router.post('/post', urlencodedParser, function(req, res) {
        postTask(req, function (err) {
            if (err)
                return (err);
        }).then(() =>{
            res.redirect("/")
        })
    });

    router.post('/delete/:id', urlencodedParser, function (req, res) {
        deleteTask(req, function (err) {
            if (err)
                return (err);
        }).then(()=>{
            res.redirect("/")
        })
    });

    router.post('/edit/:id', urlencodedParser, function (req, res) {
        editTask(req, function (err) {
            if (err)
                return (err);
        }).then(()=>{
            res.redirect("/")
        })
    });

    return router;
};


module.exports = createRouter;
